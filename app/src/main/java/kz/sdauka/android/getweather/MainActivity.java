package kz.sdauka.android.getweather;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Dauletkhan on 13.02.2015.
 */
public class MainActivity extends ActionBarActivity {
    @InjectView(R.id.cityName)
    TextView cityName;
    @InjectView(R.id.description)
    TextView description;
    @InjectView(R.id.minTemp)
    TextView minTemp;
    @InjectView(R.id.maxTemp)
    TextView maxTemp;
    @InjectView(R.id.windSpeed)
    TextView windSpeed;
    @InjectView(R.id.cloudPerc)
    TextView cloudPercent;
    @InjectView(R.id.pressure)
    TextView pressure;
    @InjectView(R.id.humidity)
    TextView humitidy;
    @InjectView(R.id.day)
    TextView day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        ButterKnife.inject(this);
        cityName.setText(getIntent().getStringExtra("name"));
        description.setText(getIntent().getStringExtra("description"));
        minTemp.setText((int) getIntent().getFloatExtra("minTemp", 0) + "°C");
        maxTemp.setText((int) getIntent().getFloatExtra("maxTemp", 0) + "°C");
        windSpeed.setText((int) getIntent().getFloatExtra("windSpeed", 0) + "m/s");
        cloudPercent.setText(getIntent().getIntExtra("cloudPercent", 0) + "%");
        pressure.setText((int) getIntent().getFloatExtra("pressure", 0) + "hPa");
        humitidy.setText((int) getIntent().getFloatExtra("humidity", 0) + "%");
        day.setText(getIntent().getStringExtra("day"));
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

}
