package kz.sdauka.android.getweather;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.UnsupportedEncodingException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import kz.sdauka.android.getweather.adapter.WeatherModelAdapter;
import kz.sdauka.android.getweather.model.Weather;
import kz.sdauka.android.getweather.util.GpsTracker;
import kz.sdauka.android.getweather.util.WeatherXmlParser;

/**
 * Created by Dauletkhan on 18.02.2015.
 */
public class ListActivity extends ActionBarActivity {
    private ProgressDialog progressDialog;
    private GpsTracker gps;
    @InjectView(R.id.listView)
    ListView listView;
    @InjectView(R.id.cityField)
    EditText cityField;
    Weather weather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);
        ButterKnife.inject(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        gps = new GpsTracker(ListActivity.this);
        RequestParams params = new RequestParams();
        if (gps.canGetLocation()) {
            params.put("lat", gps.getLatitude());
            params.put("lon", gps.getLongitude());
            params.put("mode", "xml");
            params.put("units", "metric");
            params.put("cnt", 7);
            getWeather(params, progressDialog);
//            WeatherModelAdapter weatherModelAdapter = new WeatherModelAdapter(this, weather1.getTime());
//            listView.setAdapter(weatherModelAdapter);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("icon", weather.getTime().get(position).getIcon());
                intent.putExtra("name", weather.getLocation().getCity() + " " + weather.getLocation().getCountry());
                intent.putExtra("description", weather.getTime().get(position).getDescription());
                intent.putExtra("minTemp", weather.getTime().get(position).getMinTemp());
                intent.putExtra("maxTemp", weather.getTime().get(position).getMaxTemp());
                intent.putExtra("windSpeed", weather.getTime().get(position).getWidnSpeed());
                intent.putExtra("cloudPercent", weather.getTime().get(position).getCloudPerc());
                intent.putExtra("pressure", weather.getTime().get(position).getPressure());
                intent.putExtra("humidity", weather.getTime().get(position).getHumidity());
                intent.putExtra("day", weather.getTime().get(position).getDay());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStop() {
        gps.stopUsingGPS();
        progressDialog.dismiss();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        gps.stopUsingGPS();
        progressDialog.dismiss();
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.searchBtn)
    public void searchBtn(View view) {
        String city = cityField.getText().toString();
        RequestParams params = new RequestParams();
        if (city != null && city.trim().length() > 0) {
            params.put("q", city);
            params.put("mode", "xml");
            params.put("units", "metric");
            params.put("cnt", 7);
            getWeather(params, progressDialog);
        } else Toast.makeText(getApplicationContext(), "Заполните поле", Toast.LENGTH_LONG).show();

    }

    private void getWeather(RequestParams params, final ProgressDialog progressDialog) {
        progressDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.openweathermap.org/data/2.5/forecast/daily", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String data = new String(responseBody, "UTF-8");
                    weather = WeatherXmlParser.getWeather(data, getApplicationContext());
                    WeatherModelAdapter weatherModelAdapter = new WeatherModelAdapter(getApplicationContext(), weather.getTime());
                    listView.setAdapter(weatherModelAdapter);
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(getApplicationContext(), "Произошла ошибка [JSON ответ от сервера может быть недействительным]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                // Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Запрашиваемый город не найден", Toast.LENGTH_LONG).show();
                }
                // Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Что-то пошло не так на сервере", Toast.LENGTH_LONG).show();
                }
                //Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Произошла ошибка! [Наиболее распространенные ошибки: Устройство не подключено к Интернету или удаленный сервер не запущен]", Toast.LENGTH_LONG).show();
                }
            }

        });
    }
}
