package kz.sdauka.android.getweather.model;

import java.util.List;

import kz.sdauka.android.getweather.model.weather.Time;

/**
 * Created by Dauletkhan on 14.02.2015.
 */
public class Weather {
    private Location location;
    private List<Time> time;

    public Weather() {
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Time> getTime() {
        return time;
    }

    public void setTime(List<Time> time) {
        this.time = time;
    }
}
