package kz.sdauka.android.getweather.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;
import java.util.List;

import kz.sdauka.android.getweather.R;
import kz.sdauka.android.getweather.model.weather.Time;

/**
 * Created by Dauletkhan on 18.02.2015.
 */
public class WeatherModelAdapter extends BaseAdapter {
    private List<Time> weatherList;
    private LayoutInflater layoutInflater;

    public WeatherModelAdapter(Context context, List<Time> weatherList) {
        this.weatherList = weatherList;
        this.layoutInflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return weatherList.size();
    }

    @Override
    public Object getItem(int position) {
        return weatherList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_layout, parent, false);
        }
        Time time = getWeahterById(position);
        final ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        TextView date = (TextView) view.findViewById(R.id.dateLbl);
        TextView temp = (TextView) view.findViewById(R.id.tempLbl);
        date.setText(time.getDay());
        temp.setText("Max " + time.getMaxTemp() + "°C Min " + time.getMinTemp() + "°C");
        AsyncHttpClient client = new AsyncHttpClient();
        if (time.getIcon() != null && time.getIcon().length() > 0) {
            client.get("http://openweathermap.org/img/w/" + time.getIcon() + ".png", new FileAsyncHttpResponseHandler(view.getContext().getApplicationContext()) {
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, File response) {
                    imageView.setImageBitmap(BitmapFactory.decodeFile(response.getAbsolutePath()));
                }

            });
        }
        return view;
    }


    private Time getWeahterById(int position) {
        return (Time) getItem(position);
    }
}
