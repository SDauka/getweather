package kz.sdauka.android.getweather.model;

import java.io.Serializable;

/**
 * Created by Dauletkhan on 14.02.2015.
 */
public class Location implements Serializable {
    private String country;
    private String city;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
