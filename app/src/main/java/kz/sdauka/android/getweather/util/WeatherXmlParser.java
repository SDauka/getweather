package kz.sdauka.android.getweather.util;

import android.content.Context;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import kz.sdauka.android.getweather.model.Location;
import kz.sdauka.android.getweather.model.Weather;
import kz.sdauka.android.getweather.model.weather.Time;

/**
 * Created by Dauletkhan on 18.02.2015.
 */
public class WeatherXmlParser {

    public static Weather getWeather(String data, Context context) {
        Weather weathers = new Weather();
        Location location = new Location();
        List<Time> timeList = new ArrayList<>();
        try {
            XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xppf.newPullParser();
            InputStream input = new ByteArrayInputStream(data.getBytes());
            parser.setInput(input, "utf-8");
            Time time = new Time();
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {

                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("name")) {
                    location.setCity(parser.nextText());
                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("country")) {
                    location.setCountry(parser.nextText());
                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("time")) {

                    time.setDay(parser.getAttributeValue(0));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("symbol")) {
                    time.setDescription(parser.getAttributeValue(1));
                    time.setIcon(parser.getAttributeValue(2));
                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("windSpeed")) {
                    time.setWidnSpeed(Float.parseFloat(parser.getAttributeValue(0)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("temperature")) {
                    time.setMaxTemp(Float.parseFloat(parser.getAttributeValue(1)));
                    time.setMinTemp(Float.parseFloat(parser.getAttributeValue(2)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("pressure")) {
                    time.setPressure(Float.parseFloat(parser.getAttributeValue(1)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("humidity")) {
                    time.setHumidity(Float.parseFloat(parser.getAttributeValue(0)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("clouds")) {
                    time.setCloudPerc(Integer.parseInt(parser.getAttributeValue(1)));
                    timeList.add(time);
                    time = new Time();
                }
                parser.next();
            }
            weathers.setLocation(location);
            weathers.setTime(timeList);
        } catch (Throwable t) {
            Toast.makeText(context,
                    "Ошибка при загрузке XML-документа: " + t.toString(), Toast.LENGTH_SHORT)
                    .show();
        }
        return weathers;
    }
}
